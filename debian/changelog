pyfiglet (1.0.2+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Exclude fonts-standard and fonts-contrib from our tarballs.
  * Refresh patches.
  * Drop dependency on python3-pkg-resources, no longer required.
  * Bump watch file to format 4.
  * Bump copyright years.
  * Bump Standards-Version to 4.6.2, no changes needed.
  * Update README and CHANGELOG names.

 -- Stefano Rivera <stefanor@debian.org>  Fri, 22 Sep 2023 14:08:53 +0530

pyfiglet (0.8.0+dfsg-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + python3-pyfiglet: Drop versioned constraint on python-pyfiglet in
      Replaces.
    + python3-pyfiglet: Drop versioned constraint on python-pyfiglet in Breaks.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 22:26:33 +0100

pyfiglet (0.8.0+dfsg-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Stefano Rivera ]
  * Update upstream metadata.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Thu, 02 Jun 2022 21:05:55 -0400

pyfiglet (0.8.0+dfsg-1) unstable; urgency=medium

  [ Stefano Rivera ]
  * New upstream release. (Closes: #921029)
  * Update watch file to catch "v" prefix. (Closes: #921028)
  * Relicense under MIT/Expat, following upstream.
  * Refresh patches.
  * Drop patches, superseded upstream: bad-toilet-fonts, py3k.
  * Depend on python3-pkg-resources. Our patch doesn't remove it any more.

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

 -- Stefano Rivera <stefanor@debian.org>  Sun, 05 Jan 2020 12:58:53 +0200

pyfiglet (0.7.4+dfsg-4) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).

 -- Ondřej Nový <onovy@debian.org>  Sat, 10 Aug 2019 13:16:31 +0200

pyfiglet (0.7.4+dfsg-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout

  [ Stefano Rivera ]
  * Bump debhelper compat to 11.
  * Bump Standards-Version to 4.2.1, no changes needed.
  * Bump copyright years.
  * Declare Rules-Requires-Root: no.

 -- Stefano Rivera <stefanor@debian.org>  Mon, 27 Aug 2018 11:55:20 +0100

pyfiglet (0.7.4+dfsg-2) unstable; urgency=medium

  * Patch py3k: Don't attempt to decode command line args in py3k.
  * Update depends for smoketests ADT tests. We now require figlet, as it
    contains our default font.

 -- Stefano Rivera <stefanor@debian.org>  Fri, 03 Jul 2015 22:12:24 -0700

pyfiglet (0.7.4+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * debian/watch:
    - Switch to dversionmangle and repacksuffix, now that it exists.
    - Switch from the deprecated githubredir.d.n to github's tag view.
  * figlet is now free, and in main:
    - Drop patch debian-defaults.
    - Build-Depend on figlet, for tests.
    - Recommend figlet, as well as toilet-fonts.
  * Patch bad-toilet-fonts: The test suite upstream has got stricter, but
    fails on some toilet fonts. Skip them.
  * Drop XS-Testsuite field, now unnecessary.
  * Bump copyright years.

 -- Stefano Rivera <stefanor@debian.org>  Fri, 03 Jul 2015 21:25:36 -0700

pyfiglet (0.7.2+dfsg-1) unstable; urgency=medium

  * New upstream bugfix release.
  * Refresh patches.
  * Bump Standards-Version to 3.9.6, no changes needed.

 -- Stefano Rivera <stefanor@debian.org>  Fri, 17 Oct 2014 19:33:30 -0700

pyfiglet (0.7.1+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * Bump copyright years.
  * Replace source tarball repacking logic with uscan's Files-Excluded
    support.
  * Port to pybuild.
    - Build-Depend on dh-python.
    - Remove SOURCES.txt cleanup.
    - Build for Python 3. The Python 3 package has taken over the pyfiglet
      command line tool.
  * Run upstream test suite. Build-Depends on toilet.
  * Add upstream test suite as an ADT test.
  * Bump Standards-Version to 3.9.5, no changes needed.
  * Add DEP-12 upstream metadata.

 -- Stefano Rivera <stefanor@debian.org>  Sun, 27 Jul 2014 21:12:10 +0200

pyfiglet (0.6.1+dfsg-1) unstable; urgency=low

  [ Stefano Rivera ]
  * New upstream release.
    - Includes manpage, drop our man page.
  * Update Homepage.
  * Bump Standards-Version to 3.9.4.
    - Bump debhelper Depends to 8.1 for build-{arch,indep} support.
  * Bump machine readable copyright format to 1.0.
  * Bump copyright years.
  * Bump debhelper compat level to 8.
  * Moved repack.sh into a get-packaged-orig-source rule in rules.
  * Drop SOURCES.txt from the binary package.
  * Add smoketest autopkgtest.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

 -- Stefano Rivera <stefanor@debian.org>  Sun, 26 May 2013 16:18:40 +0200

pyfiglet (0.6+dfsg-1) unstable; urgency=low

  * New upstream release.
    - Update copyright.
    - Update man page.
  * Upstream moved to github, updated watch file and get-orig-source.
  * Dropped patches superseded upstream:
    py26deprecation.diff, dir-error-path.diff, tlf-support.diff
  * font-location.diff: Look in /usr/share/figlet for fonts, rather than using
    pkg_resources.
  * Updated debian-defaults.diff.
  * Install wrapper with dh_install, as upstream has a setup.py.
  * Build-Depend on python-setuptools and clean up egg-info.
  * Move DFSG repacking into a script called by uscan.

 -- Stefano Rivera <stefanor@debian.org>  Thu, 18 Aug 2011 12:54:45 +0200

pyfiglet (0.4+dfsg-3) unstable; urgency=low

  * debian/copyright:
    - Update upstream maintainer's email address.
    - Update format.
  * Update my e-mail address.
  * Use X-Python-Version.
    - BD on python-all 2.6.5-13.
    - Specify supported versions rather than all.
  * Drop Breaks: ${python:Breaks}, no longer used by dh_python2.
  * Bump Standards-Version to 3.9.2, no changes needed.
  * debian/rules: Correct argument order for dh.
  * Patches: Update Forwarding status, upstream development has moved to
    github.
  * Correct DEP3 headers (first line of Description is the subject)

 -- Stefano Rivera <stefanor@debian.org>  Sun, 01 May 2011 22:03:51 -0400

pyfiglet (0.4+dfsg-2) unstable; urgency=low

  * Convert to dh_python2.
  * Move manpage installation from override rule to debian/manpages file.
  * Bump Standards-Version to 3.9.0, no changes needed.
  * get-orig-source: Don't leak uid and umask into source tarball and set -e.
  * Correctly subclass str, to fix a deprecation warning under Python 2.6
    (Closes: #588855)

 -- Stefano Rivera <stefano@rivera.za.net>  Mon, 12 Jul 2010 21:57:41 +0200

pyfiglet (0.4+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #564609)

 -- Stefano Rivera <stefano@rivera.za.net>  Thu, 28 Jan 2010 13:00:41 +0200
