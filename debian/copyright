Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pyfiglet
Upstream-Contact: Peter Waller <peter.waller@gmail.com>
Source: https://github.com/pwaller/pyfiglet
Files-Excluded:
 pyfiglet/fonts-contrib
 pyfiglet/fonts-standard
 doc/figfont.txt
Comment:
 The upstream for this package includes non-distributable items is the release
 tarball:
 * pyfiglet/fonts - a collection of figlet fonts from ftp.figlet.org
 * doc/figfont.txt - the Figlet specification
 Both clearly non-distributable, as described here http://bugs.debian.org/274950
 .
 Instead of the included fonts, this package can use the fonts provided by
 toilet-fonts or figlet in /usr/share/figlet.
 .
 Upstream bug: https://sourceforge.net/tracker/?func=detail&aid=2933719&group_id=200820&atid=975074

Files: *
Copyright: 2007, Christopher Jones <cjones@gmail.com>,
           2011-2023, Peter Waller <peter.waller@gmail.com>,
           2011-2023, Stefano Rivera <stefanor@debian.org>
License: Expat

Files: debian/*
Copyright: 2010-2023, Stefano Rivera <stefanor@debian.org>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
